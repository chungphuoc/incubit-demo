class ApplicationController < ActionController::Base
  helper_method :current_user

  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    else
      @current_user = nil
    end
  end

  private

  def authenticate_user!
    redirect_to root_path, alert: 'Please login first!' unless current_user
  end
end
