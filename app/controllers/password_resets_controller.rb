class PasswordResetsController < ApplicationController
  def new; end

  def create
    user = User.find_by_email(params[:email])

    user&.send_password_reset
    flash[:notice] = 'E-mail sent with password reset instructions.'
    redirect_to new_session_path
  end

  def edit
    @user = User.find_by_password_reset_token(params[:id])

    redirect_to new_session_path unless @user
  end

  def update
    @user = User.find_by_password_reset_token(params[:id])
    return redirect_to new_session_path unless @user

    if @user.password_reset_sent_at < 6.hour.ago
      flash[:notice] = 'Password reset has expired'
      redirect_to new_password_reset_path
    elsif @user.update(user_params)
      flash[:notice] = 'Password has been reset!'
      redirect_to new_session_path
    else
      render :edit
    end
  end

  private

  def user_params
    params.require(:user).permit(:password)
  end
end
