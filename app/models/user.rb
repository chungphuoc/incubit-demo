# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string
#  name                   :string
#  password_digest        :string
#  password_reset_token   :string
#  password_reset_sent_at :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
class User < ApplicationRecord
  validates :email, presence: true, uniqueness: true
  validates_length_of :password, minimum: 8
  has_secure_password

  def send_password_reset
    generate_token(:password_reset_token)
    password_reset_sent_at = Time.zone.now
    save(validate: false)

    UserMailer.forgot_password(self).deliver_now
  end

  private

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end
end
