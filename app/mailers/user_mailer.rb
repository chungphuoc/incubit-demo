class UserMailer < ApplicationMailer
  # send an email when someone register online account
  def welcome(user)
    @user = user

    mail(to: @user.email,
         subject: 'Welcome to our system.')
  rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
    Rails.logger.error(e.message)
    Rails.logger.error(e.backtrace.join("\r\n"))
  end

  def forgot_password(user)
    @user = user

    mail(to: @user.email,
         subject: 'Reset password instructions')
  rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
    Rails.logger.error(e.message)
    Rails.logger.error(e.backtrace.join("\r\n"))
  end
end
