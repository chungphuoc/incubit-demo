# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

## Ruby on Rails version
- Ruby 2.7.1
- Rails 6.0.3.2
## System dependencies
Ruby, Rails, PostgreSQL

## Configuration & Setup project
- After clone the project please run:
```
cp config/database.yml.sample config/database.yml
rails db:create
rails db:migrate
rails s
```

## How to run the test suite
- bundle exec rspec
