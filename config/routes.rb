Rails.application.routes.draw do
  get 'password_resets/new'
  get '/login', to: 'sessions#new', as: 'login'
  get '/signup', to: 'users#new', as: 'signup'
  get '/logout', to: 'sessions#destroy', as: 'logout'
  resources :users
  resources :sessions, only: %i[new create destroy]
  resources :password_resets

  root 'users#index'
end
