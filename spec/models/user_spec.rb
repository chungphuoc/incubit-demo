# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string
#  name                   :string
#  password_digest        :string
#  password_reset_token   :string
#  password_reset_sent_at :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'factory' do
    context 'true' do
      let(:user) { FactoryBot.build(:user) }
      it { expect(user).to be_valid }
    end

    context 'false' do
      let(:user) { FactoryBot.build(:user) }
      before { user.update(email: nil) }
      it { expect(user).to be_invalid }
    end
  end

  describe 'validation' do
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_uniqueness_of(:email) }
    it { is_expected.to validate_length_of(:password).is_at_least(8) }
  end
end
