require 'rails_helper'

RSpec.describe PasswordResetsController, type: :controller do
  let!(:user) { FactoryBot.create(:user, password_reset_token: '12345') }

  describe 'get #new' do
    def do_request
      get :new
    end

    context 'access to new page' do
      before do
        do_request
      end

      it { expect(response).to render_template :new }
    end
  end

  describe 'post #create' do
    let(:params) do
      { user: { email: user.email } }
    end

    def do_request(_user, params = {})
      put :create, params: params
    end

    context 'create new request success' do
      before do
        do_request(user, params)
      end

      it { expect(flash[:notice]).to eq 'E-mail sent with password reset instructions.' }
    end
  end

  describe 'put #update' do
    let(:params) do
      { user: { password: '123456789' } }
    end

    def do_request(user, params = {})
      put :update, params: params, session: { user_id: user.id }
    end

    context 'update the password success' do
      let!(:user) { FactoryBot.create(:user, password_reset_token: '12345', password_reset_sent_at: Time.now) }

      before do
        do_request(user, params.merge(id: user.password_reset_token))
      end

      it { expect(flash[:notice]).to eq 'Password has been reset!' }
    end

    context 'The token is expired' do
      let!(:user) { FactoryBot.create(:user, password_reset_token: '12345', password_reset_sent_at: 7.hours.ago) }

      before do
        do_request(user, params.merge(id: user.password_reset_token))
      end

      it { expect(flash[:notice]).to eq 'Password reset has expired' }
    end
  end

  describe 'get #edit' do
    def do_request(user, params = {})
      get :edit, params: params
    end

    context 'access to edit page' do
      before do
        do_request(user, id: user.password_reset_token)
      end

      it { expect(assigns(:user)).to eq user }
      it { expect(response).to render_template :edit }
    end
  end
end
