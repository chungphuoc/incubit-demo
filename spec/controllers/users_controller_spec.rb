require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let!(:user) { FactoryBot.create(:user) }

  describe 'get #index' do
    def do_request
      get :index
    end

    context 'access to index page' do
      context 'format html' do
        before do
          do_request
        end

        it { expect(assigns(:users)).to include(user) }
      end
    end
  end

  describe 'get #new' do
    def do_request
      get :new
    end

    context 'access to new page' do
      before do
        do_request
      end

      it { expect(response).to render_template :new }
    end
  end

  describe 'post #create' do
    let(:params) do
      { user: { name: Faker::Name.name, email: Faker::Internet.free_email, password: '12341234' } }
    end

    def do_request(_user, params = {})
      put :create, params: params
    end

    context 'create user success' do
      before do
        do_request(user, params)
      end

      it { expect(response).to redirect_to user_path(assigns(:user).id) }
      it { expect(flash[:notice]).to eq 'User was successfully created.' }
    end

    context 'Fail to create user' do
      before do
        params[:user][:email] = nil
        do_request(user, params)
      end

      it { expect(response).to render_template :new }
    end
  end

  describe 'put #update' do
    let(:params) do
      { user: { name: Faker::Name.name, password: '12341234' } }
    end

    def do_request(user, params = {})
      put :update, params: params, session: { user_id: user.id }
    end

    context 'update name success' do
      before do
        do_request(user, params.merge(id: user.id))
      end

      it { expect(response).to redirect_to user_path(user.id) }
      it { expect(flash[:notice]).to eq 'User was successfully updated.' }
      it { expect(user.reload.name).to eq params[:user][:name] }
    end

    context 'Fail to update user' do
      before do
        allow_any_instance_of(User).to receive(:save).and_return(false)
        do_request(user, params.merge(id: user.id))
      end

      it { expect(response).to render_template :edit }
    end
  end

  describe 'get #edit' do
    def do_request(user, params = {})
      get :edit, params: params, session: { user_id: user.id }
    end

    context 'access to edit page' do
      before do
        do_request(user, id: user.id)
      end

      it { expect(assigns(:user)).to eq user }
      it { expect(response).to render_template :edit }
    end
  end
end
