require 'rails_helper'

RSpec.describe UserMailer, type: :mailer do
  let(:user) { FactoryBot.create(:user, password_reset_token: Faker::Number.number) }

  describe 'send welcome mail' do
    let(:mail) { described_class.welcome(user) }

    it 'renders the headers' do
      expect(mail.subject).to match('Welcome to our system.')
    end

    it 'renders the body' do
      expect(mail.body).to include('Welcome to our amazing sign up system.')
    end
  end

  describe 'send forgot password mail' do
    let(:mail) { described_class.forgot_password(user) }

    it 'renders the headers' do
      expect(mail.subject).to match('Reset password instructions')
    end

    it 'renders the body' do
      expect(mail.body).to include('To reset your password')
    end
  end
end
